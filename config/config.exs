# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :quickstart_lutece, QuickstartLuteceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "i5Jodmq6ARofmIzkvDBWp/dVW7B8FCKr2MH9JcjEsLJ1xvwxtCu0q7TafCy5hD/f",
  render_errors: [view: QuickstartLuteceWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: QuickstartLutece.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
