defmodule QuickstartLuteceWeb.Router do
  use QuickstartLuteceWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", QuickstartLuteceWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :show_intro
    get "/create_plugin", PageController, :show_create_plugin
  end

  # Other scopes may use custom stacks.
  # scope "/api", QuickstartLuteceWeb do
  #   pipe_through :api
  # end
end
