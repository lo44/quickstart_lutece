defmodule QuickstartLuteceWeb.PageController do
  use QuickstartLuteceWeb, :controller

  def show_create_plugin(conn, _params) do
    render conn, "create_plugin.html"
  end

  def show_intro(conn, _params) do
    render conn, "intro.html"
  end
  
end
